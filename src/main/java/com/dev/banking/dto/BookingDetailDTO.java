package com.dev.banking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingDetailDTO {
	private ProductDTO product;
	private Integer quantity;
	private Double amountDetail;
}
