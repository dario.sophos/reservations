package com.dev.banking.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingDTO {
	private Long id;
	private LocalDateTime systemDate;
	private CustomerDTO customer;
	private LocalDate purchaseDate;
	private Double amount;
	private Integer status;
	private List<BookingDetailDTO> details;
}
