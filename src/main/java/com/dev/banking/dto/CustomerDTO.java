package com.dev.banking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {
	private Long id;
	private String username;
	private String password;
	private String identification;
	private String name;
	private String email;
	private String phone;
}
