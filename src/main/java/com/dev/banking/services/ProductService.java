package com.dev.banking.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.dev.banking.dto.ProductDTO;
import com.dev.banking.mappers.ProductMapper;
import com.dev.banking.repositories.ProductRepository;

@Service
public class ProductService {

	private ProductRepository productRepository;

	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public List<ProductDTO> findAll() {
		return productRepository.findAll().stream().map(ProductMapper::castToDto).collect(Collectors.toList());
	}

}
