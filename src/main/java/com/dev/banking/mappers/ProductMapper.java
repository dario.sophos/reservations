package com.dev.banking.mappers;

import java.util.Optional;

import com.dev.banking.dto.ProductDTO;
import com.dev.banking.entities.Product;

public class ProductMapper {

	public static ProductDTO castToDto(Product product) {
		return Optional.ofNullable(product).map(mapper -> new ProductDTO(mapper.getId(), mapper.getCode(),
				mapper.getName(), mapper.getDescription(), mapper.getUnitValue())).orElse(null);
	}

}
