package com.dev.banking;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.dev.banking.entities.Product;
import com.dev.banking.repositories.ProductRepository;

@SpringBootApplication
public class ReservationsApplication implements CommandLineRunner {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(ReservationsApplication.class);
	
	@Autowired
	private ProductRepository productRepository;

	public static void main(String[] args) {
		SpringApplication.run(ReservationsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("Esto es una mensaje {}", "Update");
		Product product = new Product();
		product.setCode(UUID.randomUUID().toString());
		product.setDescription("Description");
		product.setName("Fddadadadadada");
		product.setUnitValue(10.0);
		productRepository.save(product);
	}
	
	{
		LOGGER.info("Antes de ...");
	}

}
