package com.dev.banking.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dev.banking.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
