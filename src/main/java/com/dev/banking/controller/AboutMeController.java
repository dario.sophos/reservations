package com.dev.banking.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apiv1/about")
public class AboutMeController {
	
	private static final Logger logger = LoggerFactory.getLogger(AboutMeController.class); 
	
	@GetMapping
	public Principal sendMessage(Principal principal) {
		logger.info("Objeto principal {}", principal);
		return principal;
	}
	
}
