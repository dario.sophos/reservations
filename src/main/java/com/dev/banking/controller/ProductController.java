package com.dev.banking.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dev.banking.dto.ProductDTO;
import com.dev.banking.services.ProductService;

@RestController
@RequestMapping("/apiv1/products")
public class ProductController {

	private ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@GetMapping
	public List<ProductDTO> findAll() {
		return productService.findAll();
	}

}
